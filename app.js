const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const PORT = 3000;
let frenchMovies = [];

    /* Middleware */
app.use( '/public', express.static('public') ); // Path of statics files
//app.use( bodyParser.urlencoded({ extended: false }) ); // Get back body's content

    /* Config template */
app.set('views', './views'); // Folder
app.set('view engine', 'ejs'); // Template engine


    /* Routes */
    app.get('/', (requ, res) => {
        //res.send('Hello'); // .send = Return string
        res.render('index'); // .render = Return template
    });

    app.get('/movies', (req, res) => {

        const pageTitle = "Films français";

        frenchMovies= [
            { title: "Eternal sunshine", year: 2004 },
            { title: "Le fabuleux destin d'Amélie Poulain", year: 2001 },
            { title: "Léon", year: 1994 },
            { title: "Les vacances de Monsieur Hulot", year: 1953 },
            { title: "Le Roi et l'Oiseau", year: 1953 }
        ];
        res.render('movies', { movies: frenchMovies, title: pageTitle });
            // 'template', { params }
    });

    /* Form POST */
    var urlEncodedParser = bodyParser.urlencoded({ extended: false }); // Middleware used only on movies page
    app.post('/movies', urlEncodedParser, (req,res) => {

        //res.sendStatus( 201 ); // Create status (test post)
        //console.log( 'Titre : ', req.body.movieTitle);
        console.log( req.body );

        const newMovie = { 
            title: req.body.movieTitle, 
            year: req.body.movieYear
        };
  
        frenchMovies = [ ...frenchMovies, newMovie ]; // ...spread ES6
        console.log( frenchMovies );

    });

    app.get('/movies/add', (req, res) => { // Beware about routes order -> :id :title
        res.render("Formulaire : ajout de nouveau film");
    });

    app.get('/movies/:id', (req, res) => {
        const id = req.params.id; // Get params
        const title = req.params.title;
        const synopsis = req.params.synopsis; 
        //res.send(`Film n° ${id}`);
        res.render('movie-details', { 
            movieId: id,
            movieTitle: title,
            movieSynopsis: synopsis
        }); // Send parameters to the view/template
    });


    /* Infos */
app.listen( PORT, () => {
    console.log(`listening on port : ${PORT}`);
});