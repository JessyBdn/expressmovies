ExpressMovies
===================

This is an application built with Node and Express.

----------

1 - Install node modules : 
### `npm install`

2 - Start the server with :
### `npm start` or `npm run dev` (nodemon)
and open your browser at **localhost:3000**
